import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'image_slider.dart';
import 'sliding_route/slide_right_route.dart';
import 'package:flutter_minimall_test/screens/product_detail_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_minimall_test/screens/products_screen.dart';
import 'dart:async';

class ExpandableList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExpandableListState();
  }
}

const loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
    " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
    "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
    "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. "
    "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

class _ExpandableListState extends State<ExpandableList> {
  bool _isExpanded = false;

  List<bool> _isExpandedList;
  List<ExpandableController> _controllerList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isExpandedList = List.generate(5, (b) => false);
    _controllerList = List.generate(5, (controller) => ExpandableController());
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
//        appBar: AppBar(
//          title: Text("Expandable List"),
//        ),
        body: NestedScrollView(
      headerSliverBuilder: (context, value) {
        return <Widget>[
          SliverAppBar(
            title: Text("Expandable List"),
          )
        ];
      },
      body: RefreshIndicator(
        backgroundColor: Colors.black,
        color: Colors.white,
        onRefresh: refreshList,
        child: ListView(
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
            top: 5.0,
            bottom: 5.0,
          ),
          children: <Widget>[
            SizedBox(height: 8.0),
            _buildExpandableCard(
                "https://www.panora.com.tr/Pub/MallStoreItemMapImage/29.jpg",
                "https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112015/mavi_logo_0_0.png?itok=EZGDPR4-",
                "Mavi Ümraniye Çarşı Mağazası Buyaka AVM",
                "Atatürk Mah. Mavi Ümraniye Çarşı Mağazası Buyaka AVM",
                "Pantolonlarda büyük indirim! Sakın kaçırmayın! ndirim! Sakın kaçırmayın! Pantolonlarda büyük indirim! Sakın kaçırmayın! ",
                0),
            SizedBox(height: 16.0),
            _buildExpandableCard(
                "https://www.panora.com.tr/Pub/MallStoreItemMapImage/29.jpg",
                "https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112015/mavi_logo_0_0.png?itok=EZGDPR4-",
                "Mavi Ümraniye Çarşı Mağazası",
                "Atatürk Mah.",
                "Pantolonlarda büyük indirim! Sakın kaçırmayın!",
                1),
            SizedBox(height: 16.0),
            _buildExpandableCard(
                "https://www.panora.com.tr/Pub/MallStoreItemMapImage/29.jpg",
                "https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112015/mavi_logo_0_0.png?itok=EZGDPR4-",
                "Mavi Ümraniye Çarşı Mağazası",
                "Atatürk Mah.",
                "Pantolonlarda büyük indirim! Sakın kaçırmayın!",
                2),
            SizedBox(height: 16.0),
            _buildExpandableCard(
                "https://www.panora.com.tr/Pub/MallStoreItemMapImage/29.jpg",
                "https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112015/mavi_logo_0_0.png?itok=EZGDPR4-",
                "Mavi Ümraniye Çarşı Mağazası",
                "Atatürk Mah.",
                "Pantolonlarda büyük indirim! Sakın kaçırmayın!",
                3),
            SizedBox(height: 16.0),
            _buildExpandableCard(
                "https://www.panora.com.tr/Pub/MallStoreItemMapImage/29.jpg",
                "https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112015/mavi_logo_0_0.png?itok=EZGDPR4-",
                "Mavi Ümraniye Çarşı Mağazası",
                "Atatürk Mah.",
                "Pantolonlarda büyük indirim! Sakın kaçırmayın!",
                4),
            SizedBox(height: 8.0),
          ],
        ),
      ),
    ));
  }

  Widget _buildExpandableCard(String image, String avatar, String title,
      String subtitle, String content, int index) {
    return Card(
        elevation: 5.0,
        clipBehavior: Clip.antiAlias,
        child: ExpandableNotifier(
          controller: ExpandableController(_isExpanded),
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * .35,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(image),
                      fit: BoxFit.fitWidth),
                ),
              ),
              ExpandablePanel(
                tapHeaderToExpand: true,
                header: Padding(
                    padding:
                        EdgeInsets.only(top: 16.0, left: 16.0, bottom: 16.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: AspectRatio(
                              aspectRatio: 1.0,
                              child: CachedNetworkImage(
                                imageUrl: avatar,
                                placeholder: (context,url) => CircularProgressIndicator(),
                              ),
                            ),
                          ),
                          flex: 1,
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Expanded(
                          flex: 6,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                title,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.black,
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                              Text(
                                subtitle,
                                style: TextStyle(
                                  color: Colors.grey[700],
                                  fontSize: 16.0,
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                expanded: Padding(
                  padding: const EdgeInsets.only(
                      left: 8.0, right: 16.0, bottom: 4.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            content,
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 15.0, color: Colors.grey[700]),
                            overflow: TextOverflow.fade,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          InkWell(
                            onTap: () {},
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "KONUM",
                                style: TextStyle(
                                    color: Color(0xFF1565C0),
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(width: 8.0),
                          InkWell(
                            onTap: () {
//                              Navigator.push(
//                                  context,
//                                  SlideRoute(
//                                      widget: ImageSliderScreen(imageList)));
//                              setState(() {
//                                _isExpanded = false;
//                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "BİLGİ",
                                style: TextStyle(
                                    color: Color(0xFF1565C0),
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(width: 8.0),
                          InkWell(
                            onTap: () {
                              Navigator.push(context,
                                  SlideRoute(widget: ProductsScreen()));
                              setState(() {
                                _isExpanded = false;
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "ÜRÜNLER",
                                style: TextStyle(
                                    color: Color(0xFF1565C0),
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8.0)
                    ],
                  ),
                ),
                builder: (_, collapsed, expanded) {
                  return Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                  );
                },
              ),
            ],
          ),
        ));
  }

  Future<Null> refreshList() async {
    await Future.delayed(Duration(seconds: 1));
    return null;
  }
}
