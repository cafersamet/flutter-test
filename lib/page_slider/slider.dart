import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

//import 'package:photo_view/photo_view.dart';
import 'package:swipedetector/swipedetector.dart';
import 'package:flutter_minimall_test/image_slider.dart';
import 'package:flutter_minimall_test/sliding_route/slide_right_route.dart';

class CustomImageSlider extends StatefulWidget {
  final List<String> imageList;
  final BoxFit boxFit;
  final bool isHero;
  final int heroIndex;

  CustomImageSlider(
      {this.imageList,
      this.boxFit = BoxFit.cover,
      this.isHero = false,
      this.heroIndex = 0});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomImageSliderState();
  }
}

class _CustomImageSliderState extends State<CustomImageSlider> {
  int _currentIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: widget.heroIndex);
    _currentIndex = widget.heroIndex;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: PageView.builder(
              controller: _pageController,
              itemBuilder: (context, index) {
                if (widget.isHero) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ImageSliderScreen(widget.imageList, index)));
                    },
                    child: Hero(
                      tag: index,
                      child: CachedNetworkImage(
                        imageUrl: widget.imageList[index],
                        placeholder: (context, url) => Container(
                            child: Center(child: CircularProgressIndicator())),
                        fit: widget.boxFit,
//                 Container(
//                  child: PhotoView(backgroundDecoration: BoxDecoration(color: Colors.blue),
//                      minScale: 1.1,
//                      initialScale: 1.1,
//                      maxScale: 1.4,
//                      loadingChild: CircularProgressIndicator(),
//                      imageProvider: NetworkImage(widget.imageList[index])),
                      ),
                    ),
                  );
                } else {
                  return CachedNetworkImage(
                    imageUrl: widget.imageList[index],
                    placeholder: (context, url) => Container(
                        child: Center(child: CircularProgressIndicator())),
                    fit: widget.boxFit,
//                 Container(
//                  child: PhotoView(backgroundDecoration: BoxDecoration(color: Colors.blue),
//                      minScale: 1.1,
//                      initialScale: 1.1,
//                      maxScale: 1.4,
//                      loadingChild: CircularProgressIndicator(),
//                      imageProvider: NetworkImage(widget.imageList[index])),
                  );
                }
              },
              itemCount: widget.imageList.length,
              onPageChanged: (index) {
                setState(() {
                  _currentIndex = index;
                });
              },
            ),
          ),
          Positioned(
            child: Container(
              height: 48.0,
              decoration: BoxDecoration(color: Color(0x99121212)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: new List<int>.generate(
                    widget.imageList.length, (int index) => index).map((i) {
                  int index = i;
                  return Builder(builder: (BuildContext context) {
                    return Container(
                      height: 10.0,
                      width: 10.0,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: index == _currentIndex
                              ? Colors.blue[700]
                              : Colors.white),
                    );
                  });
                }).toList(),
              ),
            ),
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
          )
        ],
      ),
    );
  }
}
