import 'package:flutter/material.dart';
import 'package:flutter_minimall_test/sliding_route/slide_right_route.dart';
import 'package:flutter_minimall_test/screens/main.dart';
import 'survey_question.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {
  final List<SurveyQuestion> questionList = SurveyQuestion.questionList;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  int questionNo;
  Set<int> selectedImages;
  BorderSide tappedBorder = BorderSide(color: Colors.blue[900], width: 5.0);
  bool isImage = false;
  bool isRadio = false;
  bool isCheck = false;
  bool isInput = false;
  bool isCombo = false;
  bool _canNext = false;
  bool _canPrevious = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //SystemChrome.setEnabledSystemUIOverlays([]);
    questionNo = 0;
    selectedImages = Set();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () =>
              Navigator.of(context).pushReplacementNamed("/myhome"),
          icon: Icon(Icons.close),
        ),
        backgroundColor: Colors.blue[700],
        centerTitle: true,
        title: Text("Kullanıcı Anketi"),
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: RadialGradient(colors: [
          Colors.lightBlueAccent,
          Colors.lightBlue,
          Colors.blueAccent,
          Colors.blue[800],
        ], radius: 1.5, center: Alignment.topLeft)),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 16.0,
              ),
              width: MediaQuery.of(context).size.width,
              child: Text(
                (questionNo + 1).toString() +
                    " - " +
                    widget.questionList.elementAt(questionNo).text,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 18.0),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                child: _buildDynamicSurvey(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 16.0,
                horizontal: 16.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FloatingActionButton(
                    onPressed: () {
                      if (questionNo > 0) {
                        setState(() {
                          questionNo--;
                        });
                      }
                    },
                    child: Icon(Icons.arrow_back),
                    backgroundColor: Colors.lightBlueAccent,
                  ),
                  FloatingActionButton(
                    onPressed: _canNext ? _forwardButtonClicked : null,
                    child: Icon(Icons.arrow_forward),
                    backgroundColor:
                        _canNext ? Colors.lightBlueAccent : Colors.blueGrey,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _forwardButtonClicked() {
    if (isImage) {
      print(selectedImages.toString());
      isImage = false;
    } else if (isInput) {
      _editingControllers.forEach((TextEditingController controller) {
        print(controller.text);
      });
    }
    if (questionNo < widget.questionList.length - 1) {
      setState(() {
        questionNo++;
      });
    }
  }

  TextInputType _keyboardType(String inputType) {
    return inputType.compareTo("number") == 0
        ? TextInputType.number
        : TextInputType.text;
  }

  Widget _buildDropDownButton(List<String> choices) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: DropdownButton<String>(
        isExpanded: true,
        style: TextStyle(color: Colors.white, fontSize: 18.0),
        items: choices.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Text(
              dropDownStringItem,
              style: TextStyle(color: Colors.purple),
            ),
          );
        }).toList(),
        onChanged: (String selectedValue) {
          /*setState(() {
            this._selectedDropDownItem = selectedValue;
          });*/
        },
//        value: _selectedDropDownItem,
        iconSize: 35.0,
      ),
    );
  }

  Widget _buildDynamicSurvey() {
    SurveyQuestion surveyQuestion = widget.questionList.elementAt(questionNo);
    String questionType = surveyQuestion.type;

    // return grid view for images
    if (questionType.compareTo("image") == 0) {
      isImage = true;
      return GridView.builder(
          itemCount: surveyQuestion.questions.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 4.0,
              childAspectRatio: 1.0,
              crossAxisSpacing: 4.0),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (selectedImages.contains(index)) {
                    selectedImages.remove(index);
                  } else {
                    selectedImages.add(index);
                  }

                  selectedImages.length > 0
                      ? _canNext = true
                      : _canNext = false;
                });
              },
              child: Card(
                elevation: 2.0,
                shape: RoundedRectangleBorder(
                    side: selectedImages.contains(index)
                        ? tappedBorder
                        : BorderSide(width: 0.0),
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    Center(
                      child: CachedNetworkImage(
                        placeholder: (context,url) => CircularProgressIndicator(),
                        imageUrl: surveyQuestion.questions.elementAt(index),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
    }
    // return list view for others
    else {
      return ListView.builder(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          if (questionType.compareTo("input") == 0) {
            isInput = true;
            return _buildInputTextTile(surveyQuestion, index);
          } else if (questionType.compareTo("combo") == 0) {

            List<String> dropDown = List<String>();

            if(index == 0) {
              surveyQuestion.comboValues.forEach((Map<String, List<String>> map) {
                map.forEach((String city, List<String> towns) {
                  dropDown.add(city);
                });
              });
            }
            else{
            }

            return ListTile(
              leading: _buildDropDownButton(dropDown),
              contentPadding: EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 8.0,
              ),
            );
          } else if (questionType.compareTo("check1") == 0) {
          } else if (questionType.compareTo("radio1") == 0) {
          } else {
            return Container(child: Text("Bu sorunun tipi algılanamadı!"));
          }
        },
        itemCount: widget.questionList.elementAt(questionNo).questions.length,
      );
    }
  }

  Map<int, dynamic> enteredInputs = Map<int, dynamic>();
  List<TextEditingController> _editingControllers = List();

  Widget _buildInputTextTile(SurveyQuestion surveyQuestion, int index) {
    _editingControllers.add(TextEditingController());
    return ListTile(
      contentPadding: EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 8.0,
      ),
      leading: TextField(
        controller: _editingControllers[index],
        cursorColor: Colors.black,
        keyboardType: _keyboardType(surveyQuestion.inputType.elementAt(index)),
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black54),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          hintText: surveyQuestion.questions.elementAt(index),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _editingControllers.forEach((TextEditingController controller) {
      controller.dispose();
    });
  }
}
