import 'package:flutter/material.dart';

class CustomFab extends StatefulWidget {
  final Function onCommentPressed;
  final Function onRatePressed;

  CustomFab({@required this.onCommentPressed, @required this.onRatePressed});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomFab();
  }
}

class _CustomFab extends State<CustomFab> with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          alignment: FractionalOffset.topCenter,
          height: 64.0,
          width: 48,
          child: ScaleTransition(
            scale: CurvedAnimation(
                parent: _animationController,
                curve: Interval(0.0, 1.0, curve: Curves.easeOut)),
            child: FloatingActionButton(
              mini: true,
              heroTag: 'mail',
              backgroundColor: Colors.green,
              onPressed: () => widget.onCommentPressed(),
              child: Icon(Icons.mode_comment),
            ),
          ),
        ),
        ScaleTransition(
          scale: CurvedAnimation(
              parent: _animationController,
              curve: Interval(0.0, 1.0, curve: Curves.easeOut)),
          child: Container(
            alignment: FractionalOffset.topCenter,
            height: 64.0,
            width: 48,
            child: FloatingActionButton(
              mini: true,
              heroTag: 'favorite',
              backgroundColor: Colors.redAccent,
              onPressed: () => widget.onRatePressed(),
              child: Icon(Icons.star),
            ),
          ),
        ),
        FloatingActionButton(
          onPressed: () {
            if (_animationController.isDismissed) {
              _animationController.forward();
            } else {
              _animationController.reverse();
            }
          },
          child: Icon(Icons.more_vert),
        )
      ],
    );
  }
}
