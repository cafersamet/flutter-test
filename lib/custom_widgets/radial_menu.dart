import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart' show radians;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:math';

class RadialMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _RadialMenuState();
  }
}

class _RadialMenuState extends State<RadialMenu>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 900));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return RadialAnimation(controller: _controller);
  }
}

class RadialAnimation extends StatelessWidget {
  RadialAnimation({Key key, this.controller})
      : scale = Tween<double>(begin: 1.5, end: 0.0).animate(
            CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn)),
        translation = Tween<double>(begin: 0.0, end: 100.0).animate(
            CurvedAnimation(parent: controller, curve: Curves.elasticOut)),
        rotation = Tween<double>(begin: 0.0, end: 360.0).animate(
            CurvedAnimation(parent: controller, curve: Interval(0.0, 0.8))),
        super(key: key);

  final AnimationController controller;
  final Animation<double> scale;
  final Animation<double> translation;
  final Animation<double> rotation;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedBuilder(
        animation: controller,
        builder: (context, builder) {
          return Transform.rotate(
              angle: radians(rotation.value),
              child: Stack(alignment: Alignment.center, children: <Widget>[
                _buildButton(0,
                    color: Colors.blue[200], icon: FontAwesomeIcons.shoppingBasket),
                _buildButton(45,
                    color: Colors.green, icon: FontAwesomeIcons.star),
                _buildButton(90,
                    color: Colors.red, icon: FontAwesomeIcons.youtube),
                _buildButton(135,
                    color: Colors.indigo, icon: FontAwesomeIcons.liraSign),
                _buildButton(180,
                    color: Colors.amber[500], icon: FontAwesomeIcons.map),
                _buildButton(225,
                    color: Colors.grey, icon: FontAwesomeIcons.fedora),
                _buildButton(270,
                    color: Colors.blue, icon: FontAwesomeIcons.home),
                _buildButton(315,
                    color: Colors.pinkAccent, icon: FontAwesomeIcons.camera),
                Transform.scale(
                    scale: scale.value - 1.5,
                    child: FloatingActionButton(
                        heroTag: "close",
                        child: Icon(FontAwesomeIcons.timesCircle),
                        onPressed: _close,
                        backgroundColor: Colors.redAccent)),
                Transform.scale(
                    scale: scale.value,
                    child: FloatingActionButton(
                        heroTag: "open",
                        child: Icon(FontAwesomeIcons.solidDotCircle),
                        onPressed: _open))
              ]));
        });
  }

  _buildButton(double angle, {Color color, IconData icon}) {
    final double rad = radians(angle);
    return Transform(
        transform: Matrix4.identity()
          ..translate(
              (translation.value) * cos(rad), (translation.value) * sin(rad)),
        child: FloatingActionButton(
            heroTag: angle.toString(),
            onPressed: _close,
            child: Icon(icon),
            backgroundColor: color));
  }

  _open() {
    controller.forward();
  }

  _close() {
    controller.reverse();
  }
}
