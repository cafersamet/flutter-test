class User {
  String name;
  String surname;
  int age;
  String id;

  User({this.name, this.surname, this.age, this.id});
}
