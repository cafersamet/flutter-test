import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'models/user.dart';

enum SortType { AGE, NAME, SURNAME }

class FilterType {
  int minAge;
  int maxAge;

  FilterType({this.minAge, this.maxAge});
}

class UserListView extends StatelessWidget {
  final SortType sortType;
  final bool isDescending;
  final bool isSorted;
  final bool isFiltered;
  final FilterType filterType;

  UserListView(
      {this.sortType = SortType.NAME,
      this.isDescending = false,
      this.isSorted = false,
      this.isFiltered = false,
      this.filterType});

  //  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return ListView.builder(
//      padding: EdgeInsets.symmetric(horizontal: 4.0, vertical: 8.0),
//      shrinkWrap: true,
//      itemBuilder: (context, index) {
//        User user = userList.elementAt(index);
//        return Container(
//          margin: EdgeInsets.symmetric(vertical: 4.0, horizontal: 4.0),
//          decoration: BoxDecoration(
//              shape: BoxShape.rectangle,
//              color: Colors.orange,
//              borderRadius: BorderRadius.all(Radius.circular(4.0))),
//          child: ListTile(
//            contentPadding: EdgeInsets.all(4.0),
//            onTap: () {},
//            leading: CircleAvatar(
//              radius: 30.0,
//              backgroundColor: Colors.yellowAccent,
//              child: Text(
//                user.name.toUpperCase().substring(0, 1),
//                style: TextStyle(color: Colors.black, fontSize: 24.0),
//              ),
//            ),
//            title: Text(user.name),
//            subtitle: Text(user.surname),
//            trailing: Text(
//              user.age.toString(),
//              style: TextStyle(fontSize: 36.0, color: Colors.deepPurpleAccent),
//            ),
//          ),
//        );
//      },
//      itemCount: userList.length,
//    );
//  }

  @override
  Widget build(BuildContext context) {
    String orderBy = "name";
    if (sortType == SortType.AGE) {
      orderBy = "age";
    } else if (sortType == SortType.SURNAME) {
      orderBy = "surname";
    } else {
      orderBy = "name";
    }
    return StreamBuilder<QuerySnapshot>(
        stream: isSorted
            ? Firestore.instance
                .collection("user")
                .orderBy(orderBy, descending: isDescending)
                .snapshots()
            : Firestore.instance.collection("user").snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text("Error: ${snapshot.error}"));
          else if (!snapshot.hasData) return Center(child: Text("No data"));
          List<DocumentSnapshot> filteredList;
          if (isFiltered) {
            if (filterType.maxAge != filterType.minAge) {
              filteredList = snapshot.data.documents
                  .where((item) =>
                      item["age"] > filterType.minAge &&
                      item["age"] < filterType.maxAge)
                  .toList();
            } else {
              filteredList = snapshot.data.documents
                  .where((item) => item["age"] == filterType.minAge)
                  .toList();
            }
          } else {
            filteredList = snapshot.data.documents;
          }

          final int userCount = filteredList.length;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return CircularProgressIndicator();
            default:
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: userCount,
                  itemBuilder: (_, index) {
                    final DocumentSnapshot document = filteredList[index];
                    final User user = User();
                    user.id = document.documentID;
                    user.name = document["name"];
                    user.age = document["age"];
                    user.surname = document["surname"];
                    return Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
                      decoration: ShapeDecoration(
                          color:
                              index % 2 == 0 ? Colors.black38 : Colors.black12,
                          shape: BeveledRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)))),
                      child: ListTile(
                          contentPadding: EdgeInsets.all(4.0),
                          onTap: () {
                            Navigator.pushNamed(context, "/editUser",
                                arguments: user);
                          },
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundColor: Colors.yellowAccent,
                            child: Text(
                              user.name.toUpperCase().substring(0, 1) +
                                  user.surname.toUpperCase().substring(0, 1),
                              style: TextStyle(
                                  color: Colors.black, fontSize: 24.0),
                            ),
                          ),
                          title: Text(user.name),
                          subtitle: Text(user.surname),
                          trailing: Text(
                            user.age.toString(),
                            style: TextStyle(
                                fontSize: 36.0,
                                color: index % 2 == 0
                                    ? Colors.black38
                                    : Colors.black38),
                          )),
                    );
                  });
          }
        });
  }
}
