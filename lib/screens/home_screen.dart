import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import '../progress_dialog.dart';
import 'package:flutter/gestures.dart';
import '../presentations/custom_icons_icons.dart';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  List<String> _cities = [
    "Adalar",
    "Arnavutköy",
    "Başakşehir",
    "Şile",
    "Üsküdar"
  ];

  List<String> _popups = [
    "Profile",
    "Settings",
    "Sign Out",
  ];

  final dayList = [
    "Pazartesi",
    "Salı",
    "Çarşamba",
    "Perşembe",
    "Cuma",
    "Cumartesi",
    "Pazar",
  ];

  final tableBoxDecoration = BoxDecoration(color: Colors.blue);

  var _selectedDropDownItem;
  ProgressDialog _progressDialog;
  bool _loading = false;
  bool _checkboxValue;
  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checkboxValue = false;
    _selectedDropDownItem = _cities.elementAt(0);
    _progressDialog = ProgressDialog();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue),
        title: Text("Minimall Testing"),
        leading: IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {},
          tooltip: "Close",
        ),
        backgroundColor: Colors.green,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: _choiceAction,
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                  const PopupMenuItem<String>(
                    value: "Profile",
                    child: Text('Working a lot harder'),
                  ),
                  const PopupMenuDivider(
                    height: 3.0,
                  ),
                  const PopupMenuItem<String>(
                    value: "Settings",
                    child: Text('Being a lot smarter'),
                  ),
                  const PopupMenuDivider(
                    height: 3.0,
                  ),
                  const PopupMenuItem<String>(
                    value: "Signout",
                    child: Text('Being a self-starter'),
                  ),
                ],
          ),
          PopupMenuButton<String>(
            itemBuilder: (BuildContext context) {
              return _popups.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
            onSelected: _choiceAction,
          )
        ],
      ),
      body: _buildHomeBody(),
    );
  }

  Widget _buildHomeBody() {
    return Stack(
      children: <Widget>[
        Builder(
            builder: (context) => GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Center(
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        decoration: BoxDecoration(color: Colors.green[400]),
                        child: Form(
                            key: _keyForm,
                            child: ListView(children: <Widget>[
                              _buildEmailTextField(),
                              _buildPasswordTextField(),
                              _buildBottomSide()
                              //_buildDropDownButton(),
                            ])))))),
        _progressDialog,
      ],
    );
  }

  Widget _buildEmailTextField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          labelStyle: TextStyle(color: Colors.white),
          enabledBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          focusedBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          labelText: "E-Posta",
          icon: Icon(
            Icons.email,
            color: Colors.white,
          )),
      validator: (String value) {
        if (value.isEmpty)
          return "E-Posta alanını doldurmak zorunludur!";
        else if (!value.contains("@") || !value.endsWith(".com")) {
          return "E-Posta yı uygun formatta giriniz!";
        }
      },
      onSaved: (String value) {
        print("E-posta kaydedildi: $value");
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      obscureText: false,
      decoration: InputDecoration(
        labelStyle: TextStyle(color: Colors.white),
        hintText: "At least 8 character",
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        labelText: "Şifre",
        icon: Icon(
          Icons.vpn_key,
          color: Colors.white,
        ),
      ),
      validator: (String value) {
        if (value.isEmpty)
          return "Lütfen parolayı giriniz!";
        else if (value.length < 8 || value.length > 16)
          return "Parolanız en az 8 en fazla 16 karakter olmalıdır!";
        else if (!RegExp(r"^[a-zA-Z0-9_-]+$").hasMatch(value))
          return ("Lütfen uygun karakterlere sahip bir parola giriniz!");
      },
      onSaved: (String value) {
        print("Şifre kaydedildi: $value");
      },
    );
  }

  Widget _buildBottomSide() {
    return Container(
        child: Column(children: <Widget>[
      SizedBox(height: 32.0),
      Divider(color: Colors.black, height: 10.0),
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
        Container(
            child: InkWell(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("2D Flaree!")),
                onTap: () => Navigator.of(context).pushNamed("/flare"))),
        Container(
            child: InkWell(
                child: Padding(
                    padding: EdgeInsets.all(8.0), child: Text("Radial Menu")),
                onTap: () => Navigator.of(context).pushNamed("/radial")))
      ]),
      Divider(color: Colors.black, height: 10.0),
      MaterialButton(
          child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text("GİRİŞ YAP", textScaleFactor: 1.2)),
          onPressed: () => _submitForm(context),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          color: Colors.pinkAccent[400]),
      RawMaterialButton(
          child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Icon(CustomIcons.gplus),
                SizedBox(width: 16.0),
                Text("ile GİRİŞ YAP", textScaleFactor: 1.2)
              ])),
          onPressed: () => Navigator.pushNamed(context, "/localAuth"),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          fillColor: Colors.orangeAccent[400]),
      RaisedButton(
          color: Colors.blueAccent[400],
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Icon(Icons.map), Text("Sliver")],
            ),
          ),
          onPressed: () => Navigator.pushNamed(context, "/sliver"),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)))),
      OutlineButton.icon(
          borderSide: BorderSide(
              color: Colors.purple, width: 5.0, style: BorderStyle.solid),
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext builder) {
                  return AlertDialog(
                      contentPadding: EdgeInsets.all(16.0),
                      content: Table(
                          border: TableBorder.all(
                              width: 1.0, color: Colors.indigoAccent),
                          children: [
                            _tableRow(0, "10:00", "22:00"),
                            _tableRow(1, "10:00", "22:00"),
                            _tableRow(2, "10:00", "22:00"),
                            _tableRow(3, "10:00", "22:00"),
                            _tableRow(4, "10:00", "22:30"),
                            _tableRow(5, "10:00", "22:30"),
                            _tableRow(6, "10:00", "22:30")
                          ]),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () => Navigator.pop(context),
                            child:
                                Text("Tamam", style: TextStyle(fontSize: 15.0)))
                      ]);
                });
          },
          icon: Icon(Icons.outlined_flag, color: Colors.blue),
          label: Text("Working Hours", style: TextStyle(color: Colors.blue))),
      FlatButton(
          onPressed: () => Navigator.of(context).pushNamed("/expandable"),
          child: Text("Expandable List"),
          textColor: Colors.deepPurple),
      RichText(
          text: TextSpan(children: [
        TextSpan(
          text: 'This is no Link, ',
          style: TextStyle(color: Colors.black),
        ),
        TextSpan(
            text: 'but this is',
            style: TextStyle(
                color: Colors.blue[700], decoration: TextDecoration.underline),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                Navigator.pushNamed(context, "/webView");
              })
      ])),
      RaisedButton(
          child: Text("Lets Firebase"),
          onPressed: () {
            Navigator.pushNamed(context, "/firebase");
          }),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            children: <Widget>[
              RaisedButton(
                onPressed: () => _selectDate(context),
                child: Text("Select Date"),
                color: Colors.indigo,
              ),
              Text("Date ${_date.toString()}")
            ],
          ),
          Column(
            children: <Widget>[
              RaisedButton(
                onPressed: () => _selectTime(context),
                child: Text("Select Time"),
                color: Colors.indigo,
              ),
              Text("Time ${_time.toString()}")
            ],
          ),
        ],
      )
    ]));
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2017),
        lastDate: DateTime(2021));

    if (picked != null && picked != _date) {
      print("Date Selected");
      setState(() {
        _date = picked;
      });
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _time);

    if (picked != null && picked != _time) {
      print("Time Selected");
      setState(() {
        _time = picked;
      });
    }
  }

  Widget _buildDropDownButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: DropdownButton<String>(
        isExpanded: true,
        style: TextStyle(color: Colors.white, fontSize: 18.0),
        items: _cities.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Text(
              dropDownStringItem,
              style: TextStyle(color: Colors.purple),
            ),
          );
        }).toList(),
        onChanged: (String selectedValue) {
          setState(() {
            this._selectedDropDownItem = selectedValue;
          });
        },
        value: _selectedDropDownItem,
        iconSize: 35.0,
      ),
    );
  }

  Future<bool> _submitForm(BuildContext context) async {
//    if (!_keyForm.currentState.validate()) {
//      return false;
//    }

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      _showSnackBar(context, "İnternet bağlantınızı kontrol edin!");
      return false;
    }
    _keyForm.currentState.save();

    showProgressDialog();
    //authentication processes
    await Future.delayed(Duration(seconds: 1));
    dismissProgressDialog();

    Navigator.of(context).pushReplacementNamed("/home");

    return true;
  }

  void _choiceAction(String value) {
    print(value);
  }

  void _showSnackBar(BuildContext context, String message) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(SnackBar(
      duration: Duration(seconds: 4),
      content: Text(message),
      action: SnackBarAction(
          label: "Tamam", onPressed: scaffold.hideCurrentSnackBar),
    ));
  }

  TableRow _tableRow(int index, String openingHour, String closingHour) {
    int weekDay = DateTime.now().toLocal().weekday;
    return TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(child: Text(dayList[index])),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(child: Text(openingHour + "-" + closingHour)),
        )
      ],
      decoration: index + 1 == weekDay ? tableBoxDecoration : null,
    );
  }

  void showProgressDialog() {
    setState(() {
      if (!_loading) {
        _progressDialog.state.show();
      }
      _loading = !_loading;
    });
  }

  void dismissProgressDialog() {
    setState(() {
      if (_loading) {
        _progressDialog.state.dismiss();
      }
      _loading = !_loading;
    });
  }
}
