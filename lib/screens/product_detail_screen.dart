import 'package:flutter/material.dart';
import '../page_slider/slider.dart';
import '../presentations/custom_icons_icons.dart';
import '../sliding_route/slide_right_route.dart';
import '../image_slider.dart';
import '../custom_widgets/custom_fab.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProductDetailScreen extends StatefulWidget {
  final List<String> imageList;
  final List<String> colorList;

  ProductDetailScreen({@required this.imageList, @required this.colorList});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductDetailScreenState();
  }
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  final double cardElevation = 5.0;
  final double cardMargin = 8.0;
  final double cardPadding = 10.0;
  final Color cardColor = Colors.white;
  final double sizeBoxHeight = 50.0;
  final double iconTitleMargin = 4.0;
  final double iconSize = 22.0;

  String _dropDownSelectedItem;

  Map<String, Color> colorMap = {
    "Kırmızı": Colors.red,
    "Sarı": Colors.yellow,
    "Mavi": Colors.blue,
    "Yeşil": Colors.green
  };

  Map<String, bool> sizeMap = {
    "XS": false,
    "S": true,
    "M": false,
    "L": true,
    "XL": true
  };

  BuildContext currentContext;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.colorList.length > 0)
      _dropDownSelectedItem = widget.colorList[0];
  }

  @override
  Widget build(BuildContext context) {
    currentContext = context;

    return Scaffold(
      floatingActionButton: CustomFab(
        onCommentPressed: _onCommentPressed,
        onRatePressed: _onRatePressed,
      ),
      appBar: AppBar(
        backgroundColor: Color(0xFF121212),
        title: Text("Ürün Detayı"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
        ],
      ),
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Stack(children: <Widget>[
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: AspectRatio(
                      aspectRatio: .8,
                      child: CustomImageSlider(isHero: true,
                        imageList: widget.imageList,
                        boxFit: BoxFit.fitWidth,
                      ))),
              Positioned(
                  top: 8.0,
                  right: 8.0,
                  child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 3.0, color: Colors.grey)),
                      child: CircleAvatar(
                          radius: 32.0,
                          backgroundImage: CachedNetworkImageProvider(
                              "https://upload.wikimedia.org/wikipedia/en/thumb/d/de/USPoloAssn_logo.jpg/220px-USPoloAssn_logo.jpg",
                              scale: .5))))
            ]),
            _buildMainCard(),
            _buildColorAndSizeCard(),
            _buildModelCard(),
            _buildProductInfoCard(),
          ],
        ),
      ),
    );
  }

  void _onCommentPressed() {
    print("Comment");
  }

  void _onRatePressed() {
    showDialog(
        context: currentContext,
        builder: (context) {
          RatingWidget ratingWidget = new RatingWidget();
          return AlertDialog(
              title: Text("Bu ürünü değerlendirin!"),
              content: SizedBox(height: 50, child: ratingWidget),
              actions: <Widget>[
                FlatButton(
                    child: Text("İPTAL",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    onPressed: () => Navigator.pop(context)),
                FlatButton(
                    child: Text("ONAYLA",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    onPressed: () {
                      print(ratingWidget.ratingScore);
                      Navigator.pop(context);
                    })
              ]);
        });
  }

  Widget _buildMainCard() {
    return Card(
        color: cardColor,
        elevation: cardElevation,
        margin: EdgeInsets.all(cardMargin),
        child: Padding(
            padding: EdgeInsets.all(cardPadding),
            child: Stack(children: <Widget>[
              Positioned(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                    Row(children: <Widget>[
                      Icon(CustomIcons.male, color: Colors.blue, size: 18.0),
                      SizedBox(width: iconTitleMargin),
                      Text("Regular Triko Kazak",
                          style: TextStyle(fontSize: 18.0),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis)
                    ]),
                    SizedBox(height: 8.0),
                    Row(children: <Widget>[
                      Container(
                          width: 160.0,
                          child: SmoothStarRating(
                              allowHalfRating: true,
                              size: 32.0,
                              color: Colors.lightBlue,
                              borderColor: Colors.grey,
                              starCount: 5,
                              rating: 4.9)),
                      SizedBox(width: 8.0),
                      Text(
                        "4.8",
                        style: TextStyle(fontSize: 25, color: Colors.black),
                      )
                    ]),
                    SizedBox(height: 8.0),
                    Row(children: <Widget>[
                      Text("Toplam: 200",
                          style:
                              TextStyle(fontSize: 12.0, color: Colors.black)),
                      Icon(Icons.person, size: 15.0, color: Colors.black)
                    ])
                  ])),
              Positioned(
                  right: 0.0,
                  bottom: 0.0,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "%50",
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.0, vertical: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.blue[800]),
                        ),
                        SizedBox(width: 8.0),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Stack(children: <Widget>[
                                Positioned(
                                    top: 11.0,
                                    bottom: 12.0,
                                    left: 0.0,
                                    right: 0.0,
                                    child: Container(
                                        decoration:
                                            BoxDecoration(color: Colors.grey))),
                                Row(
                                  children: <Widget>[
                                    Icon(CustomIcons.lira,
                                        color: Colors.grey, size: 21.0),
                                    RichText(
                                        maxLines: 1,
                                        text: TextSpan(children: [
                                          TextSpan(
                                            text: '199,',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 21.0),
                                          ),
                                          TextSpan(
                                            text: '99',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 16.0),
                                          )
                                        ]))
                                  ],
                                ),
                              ]),
                              SizedBox(height: 4.0),
                              Row(children: <Widget>[
                                Icon(CustomIcons.lira,
                                    size: 21.0, color: Colors.green),
                                RichText(
                                    maxLines: 1,
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text: '99,',
                                          style: TextStyle(
                                              color: Colors.green[700],
                                              fontSize: 21.0)),
                                      TextSpan(
                                          text: '99',
                                          style: TextStyle(
                                              color: Colors.green[700],
                                              fontSize: 16.0))
                                    ]))
                              ])
                            ])
                      ]))
            ])));
  }

  Widget _buildColorAndSizeCard() {
    return Card(
        color: cardColor,
        elevation: cardElevation,
        margin: EdgeInsets.all(cardMargin),
        child: Padding(
          padding: EdgeInsets.all(cardPadding),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.palette,
                          color: Colors.black54, size: iconSize),
                      SizedBox(width: iconTitleMargin),
                      Text("RENK",
                          style: TextStyle(fontSize: 22.0, color: Colors.black))
                    ]),
                SizedBox(height: 4.0),
                _buildDropDownButton(widget.colorList),
                SizedBox(
                  height: 16.0,
                ),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.accessibility,
                          color: Colors.black54, size: iconSize),
                      SizedBox(width: iconTitleMargin),
                      Text("BEDEN",
                          style: TextStyle(fontSize: 22.0, color: Colors.black))
                    ]),
                SizedBox(height: 4.0),
                GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: sizeBoxHeight,
                        childAspectRatio: 1.0,
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0),
                    itemCount: sizeMap.length,
                    itemBuilder: (context, index) {
                      String sizeText = sizeMap.keys.toList().elementAt(index);
                      Color textColor =
                          sizeMap[sizeText] ? Colors.black87 : Colors.grey;
                      return CustomPaint(
                        size: Size(sizeBoxHeight, sizeBoxHeight),
                        painter: sizeMap[sizeText] ? null : CrossPainter(),
                        child: Container(
                          child: Center(
                              child: Stack(
                            children: <Widget>[
                              Text(
                                sizeText,
                                style:
                                    TextStyle(fontSize: 20, color: textColor),
                              ),
                            ],
                          )),
                          decoration: BoxDecoration(
                              border: Border.all(width: 1, color: textColor)),
                        ),
                      );
                    })
              ]),
        ));
  }

  Widget _buildModelCard() {
    String modelSize = "L";
    return Card(
      color: cardColor,
      margin: EdgeInsets.all(cardMargin),
      elevation: cardElevation,
      child: Padding(
        padding: EdgeInsets.all(cardPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.accessibility_new,
                      color: Colors.black54, size: iconSize),
                  SizedBox(width: iconTitleMargin),
                  Text("MODEL BİLGİLERİ",
                      style: TextStyle(fontSize: 22.0, color: Colors.black))
                ]),
            SizedBox(height: 8.0),
            Center(
              child: Wrap(
                spacing: 15.0,
                alignment: WrapAlignment.spaceAround,
                runSpacing: 10,
                children: <Widget>[
                  _modelCircleBox(185, "Boy"),
                  _modelCircleBox(85, "Kilo"),
                  _modelCircleBox(100, "Göğüs"),
                  _modelCircleBox(70, "Bel"),
                  _modelCircleBox(100, "Basen"),
                ],
              ),
            ),
            SizedBox(height: 8.0),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                text: 'Manken ',
                style: TextStyle(color: Colors.black, fontSize: 14),
              ),
              TextSpan(
                text: modelSize,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14),
              ),
              TextSpan(
                text: ' beden ürün giymektedir.',
                style: TextStyle(color: Colors.black, fontSize: 14),
              ),
            ])),
          ],
        ),
      ),
    );
  }

  Widget _buildProductInfoCard() {
    String productCode = "GL041_801472_0123";
    String productFabric = "%100 Pamuk";
    return Card(
        elevation: cardElevation,
        margin: EdgeInsets.all(cardMargin),
        color: cardColor,
        child: Padding(
            padding: EdgeInsets.all(cardPadding),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(CustomIcons.product_hunt,
                        color: Colors.black54, size: iconSize),
                    SizedBox(width: iconTitleMargin),
                    Text("ÜRÜN BİLGİLERİ",
                        style: TextStyle(fontSize: 22.0, color: Colors.black))
                  ]),
              SizedBox(height: 8.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(productCode,
                      style: TextStyle(fontSize: 18.0, color: Colors.black)),
                  Text(
                    "Ürün Kodu",
                    style: TextStyle(fontSize: 18.0, color: Colors.grey[400]),
                  )
                ],
              ),
              SizedBox(height: 8.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(productFabric,
                      style: TextStyle(fontSize: 18.0, color: Colors.black)),
                  Text(
                    "Ürün Kumaşı",
                    style: TextStyle(fontSize: 18.0, color: Colors.grey[400]),
                  )
                ],
              ),
            ])));
  }

  Widget _buildDropDownButton(List<String> choices) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white, border: Border.all(color: Colors.black26)),
        padding: EdgeInsets.only(left: 8.0),
        child: DropdownButton<String>(
            isExpanded: true,
            isDense: true,
            style: TextStyle(color: Colors.white, fontSize: 18.0),
            items: choices.map((String dropDownStringItem) {
              return DropdownMenuItem<String>(
                  value: dropDownStringItem,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        dropDownStringItem,
                        maxLines: 1,
                        style: TextStyle(color: Colors.black54, fontSize: 15.0),
                      ),
                      SizedBox(width: 4.0),
                      Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1, color: Colors.blueGrey),
                              shape: BoxShape.circle,
                              color: colorMap[dropDownStringItem])),
                    ],
                  ));
            }).toList(),
            onChanged: (String selectedValue) {
              setState(() {
                _dropDownSelectedItem = selectedValue;
              });
            },
            value: _dropDownSelectedItem,
            iconSize: 35.0));
  }

  Widget _modelCircleBox(int value, String text) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              height: 60,
              width: 60,
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.black87),
              child: Center(
                child: Text(value.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 20.0)),
              )),
          SizedBox(
            height: 4.0,
          ),
          Text(text, style: TextStyle(fontSize: 16.0, color: Colors.black))
        ]);
  }
}

class CrossPainter extends CustomPainter {
  Paint _paint;
  Size size;

  CrossPainter() {
    _paint = Paint()
      ..color = Colors.grey
      ..strokeWidth = 2.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(Offset(0, 0), Offset(size.width, size.height), _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

class RatingWidget extends StatefulWidget {
  double rateValue = 5.0;
  double ratingScore = 5.0;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _RatingWidgetState();
  }
}

class _RatingWidgetState extends State<RatingWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      alignment: Alignment.centerLeft,
      child: SmoothStarRating(
        allowHalfRating: true,
        onRatingChanged: (value) {
          widget.rateValue = value;
          widget.ratingScore = _roundRating(value);
          setState(() {});
        },
        starCount: 5,
        rating: widget.rateValue,
        size: 40.0,
        color: Colors.lightBlue,
        borderColor: Colors.grey,
      ),
    );
  }

  double _roundRating(double rating) {
    if (rating > 0.0 && rating < 0.5) {
      return 0.5;
    } else if (rating > .5 && rating < 1.0) {
      return 1.0;
    } else if (rating > 1.0 && rating < 1.5) {
      return 1.5;
    } else if (rating > 1.5 && rating < 2.0) {
      return 2.0;
    } else if (rating > 2.0 && rating < 2.5) {
      return 2.5;
    } else if (rating > 2.5 && rating < 3.0) {
      return 3.0;
    } else if (rating > 3.0 && rating < 3.5) {
      return 3.5;
    } else if (rating > 3.5 && rating < 4.0) {
      return 4.0;
    } else if (rating > 4.0 && rating < 4.5) {
      return 4.5;
    } else if (rating > 4.5 && rating < 5.0) {
      return 5.0;
    } else {
      return rating;
    }
  }
}
