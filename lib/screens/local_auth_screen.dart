import 'package:local_auth/local_auth.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class LocalAuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LocalAuthScreenState();
  }
}

class _LocalAuthScreenState extends State<LocalAuthScreen> {
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  bool _canCheckBiometric = false;
  String authorizedOrNot = "Not Authorized";
  List<BiometricType> _availableBiometricTypes = List<BiometricType>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text("Local Authentication")),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Text("Can we check Biometric : $_canCheckBiometric"),
              RaisedButton(
                onPressed: _checkBiometric,
                color: Colors.deepOrangeAccent,
                child: Text("Check Biometric"),
              ),
              Text(
                  "List of Biometric Types : ${_availableBiometricTypes.toString()}"),
              RaisedButton(
                onPressed: _canCheckBiometric ? _getListOfBiometricTypes : null,
                color: Colors.green,
                child: Text("List of Biometric Types"),
              ),
              Text("Authorized : $authorizedOrNot"),
              RaisedButton(
                onPressed: _canCheckBiometric ? _authorizeNow : null,
                color: Colors.blue,
                child: Text(authorizedOrNot),
              ),
            ])));
  }

  Future<void> _checkBiometric() async {
    bool canCheckBiometric = false;
    try {
      canCheckBiometric = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _canCheckBiometric = canCheckBiometric;
    });
  }

  Future<void> _getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometricTypes = listOfBiometrics;
    });
  }

  Future<void> _authorizeNow() async {
    bool isAuthorized = false;
    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
          localizedReason: "Uygulamaya giriş yapın",
          useErrorDialogs: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    print(isAuthorized.toString());

    setState(() {
      if (isAuthorized) {
        authorizedOrNot = "Authorized";
      } else {
        authorizedOrNot = "Not Authorized";
      }
    });
  }
}
