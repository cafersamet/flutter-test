import 'package:flutter/material.dart';
import 'package:flutter_minimall_test/sliding_route/slide_right_route.dart';
import 'main.dart';
import '../survey_question.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SurveyScreen extends StatefulWidget {
  final List<SurveyQuestion> questionList = SurveyQuestion.questionList;
  final int surveyQuestionNo;

  SurveyScreen(this.surveyQuestionNo);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SurveyScreenState();
  }
}

class _SurveyScreenState extends State<SurveyScreen> {
  int questionNo;
  Set<int> selectedImages;
  BorderSide tappedBorder = BorderSide(color: Colors.blue[900], width: 5.0);
  bool isImage = false;
  bool isRadio = false;
  bool isCheck = false;
  bool isInput = false;
  bool isCombo = false;
  bool _canNext = false;
  bool _canPrevious = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //SystemChrome.setEnabledSystemUIOverlays([]);
    questionNo = widget.surveyQuestionNo;
    selectedImages = Set();
    if (widget.questionList.elementAt(questionNo).type == "combo") {
      _selectedComboList[0] = widget.questionList
          .elementAt(questionNo)
          .comboValues
          .elementAt(0)
          .keys
          .elementAt(0);
      print("First Combo Selected" + _selectedComboList.elementAt(0));
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
//      appBar: AppBar(
//        leading: IconButton(
//          onPressed: () =>
//              Navigator.of(context).pushReplacementNamed("/myhome"),
//          icon: Icon(Icons.close),
//        ),
//        backgroundColor: Colors.blue[700],
//        centerTitle: true,
//        title: Text("Kullanıcı Anketi"),
//      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              gradient: RadialGradient(colors: [
            Colors.lightBlueAccent,
            Colors.lightBlue,
            Colors.blueAccent,
            Colors.blue[800],
          ], radius: 1.5, center: Alignment.topLeft)),
          child: Column(
            children: <Widget>[
              Material(
                color: Colors.transparent,
                child: Container(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () =>
                        Navigator.of(context).pushReplacementNamed("/myhome"),
                    iconSize: 24.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 16.0,
                ),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  (questionNo + 1).toString() +
                      " - " +
                      widget.questionList.elementAt(questionNo).text,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                  child: _buildDynamicSurvey(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 16.0,
                  horizontal: 16.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FloatingActionButton(
                      onPressed: _backButtonClicked,
                      child: Icon(Icons.arrow_back),
                      backgroundColor: Colors.lightBlueAccent,
                    ),
                    FloatingActionButton(
                      onPressed: _canNext ? _forwardButtonClicked : null,
                      child: Icon(Icons.arrow_forward),
                      backgroundColor:
                          _canNext ? Colors.lightBlueAccent : Colors.blueGrey,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _forwardButtonClicked() {
    if (isImage) {
      print(selectedImages.toString());
      isImage = false;
    } else if (isInput) {
      int index = 0;
      _editingControllers.forEach((TextEditingController controller) {
        enteredInputs[index] = controller.text;
        index++;
      });
      print(enteredInputs.toString());
    } else if (isRadio) {
      print(widget.questionList
          .elementAt(questionNo)
          .questions
          .elementAt(selectedRadio)
          .toString());
    } else if (isCheck) {
      print(selectedCheckBoxes.toString());
    } else if (isCombo) {
      print(_selectedComboList.toString());
    }
    if (questionNo < widget.questionList.length - 1) {
      questionNo++;
      Navigator.pushReplacement(
          context, SlideRoute(widget: SurveyScreen(questionNo)));
    }
  }

  void _backButtonClicked() {
    if (questionNo > 0) {
      questionNo--;
      Navigator.pushReplacement(context,
          SlideRoute(widget: SurveyScreen(questionNo), isRight: false));
    }
  }

  TextInputType _keyboardType(String inputType) {
    return inputType.compareTo("number") == 0
        ? TextInputType.number
        : TextInputType.text;
  }

  Widget _buildDropDownButton(List<String> choices, int index) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: DropdownButton<String>(
        isExpanded: true,
        style: TextStyle(color: Colors.white, fontSize: 18.0),
        items: choices.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Text(
              dropDownStringItem,
              maxLines: 1,
              style: TextStyle(color: Colors.purple),
            ),
          );
        }).toList(),
        onChanged: (String selectedValue) {
          if (index == 0) {
            setState(() {
              _isFirstComboSelected = true;
              selectedComboItemIndex = choices.indexOf(selectedValue);
              _selectedComboList[0] = selectedValue;
            });
          } else {
            setState(() {
              _canNext = true;
              _selectedComboList[1] = selectedValue;
              print("selected: " + _selectedComboList.elementAt(1));
            });
          }
        },
        value: _comboBoxSelector(index),
        iconSize: 35.0,
      ),
    );
  }

  var _selectedComboList = ["", ""];
  int selectedRadio = -1;
  Set<int> selectedCheckBoxes = Set();
  bool _isFirstComboSelected = false;
  int selectedComboItemIndex = -1;

  Widget _buildDynamicSurvey() {
    SurveyQuestion surveyQuestion = widget.questionList.elementAt(questionNo);
    String questionType = surveyQuestion.type;

    // return grid view for images
    if (questionType.compareTo("image") == 0) {
      isImage = true;
      return GridView.builder(
          itemCount: surveyQuestion.questions.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 4.0,
              childAspectRatio: 1.0,
              crossAxisSpacing: 4.0),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (selectedImages.contains(index)) {
                    selectedImages.remove(index);
                  } else {
                    selectedImages.add(index);
                  }

                  selectedImages.length > 0
                      ? _canNext = true
                      : _canNext = false;
                });
              },
              child: Card(
                elevation: 2.0,
                shape: RoundedRectangleBorder(
                    side: selectedImages.contains(index)
                        ? tappedBorder
                        : BorderSide(width: 0.0),
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    Center(
                      child: CachedNetworkImage(
                        placeholder: (context,url) => CircularProgressIndicator(),
                        imageUrl: surveyQuestion.questions.elementAt(index),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
    }
    // return list view for others
    else {
      return ListView.builder(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          if (questionType.compareTo("input") == 0) {
            isInput = true;
            return _buildInputTextTile(surveyQuestion, index);
          } else if (questionType.compareTo("combo") == 0) {
            isCombo = true;

            if (index == 0) {
              List<String> dropDown = List<String>();
              surveyQuestion.comboValues
                  .forEach((Map<String, List<String>> map) {
                map.forEach((String city, List<String> towns) {
                  dropDown.add(city);
                });
              });

              return ListTile(
                leading: _buildDropDownButton(dropDown, index),
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 8.0,
                ),
              );
            } else {
              List<String> dropDown = List<String>();
              if (_isFirstComboSelected) {
                dropDown = surveyQuestion.comboValues
                    .elementAt(selectedComboItemIndex)
                    .values
                    .elementAt(0)
                    .toList();

                if (_selectedComboList.elementAt(1) == "")
                  _selectedComboList[1] = dropDown.elementAt(0);
                return ListTile(
                  leading: _buildDropDownButton(dropDown, index),
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: 8.0,
                    vertical: 8.0,
                  ),
                );
              } else {
                return Container();
              }
            }
          } else if (questionType.compareTo("check") == 0) {
            isCheck = true;
            return CheckboxListTile(
                title: Text(surveyQuestion.questions.elementAt(index)),
                value: selectedCheckBoxes.contains(index),
                onChanged: (bool value) {
                  setState(() {
                    value
                        ? selectedCheckBoxes.add(index)
                        : selectedCheckBoxes.remove(index);

                    _canNext = selectedCheckBoxes.length > 0;
                  });
                });
          } else if (questionType.compareTo("radio") == 0) {
            isRadio = true;
            return RadioListTile(
              groupValue: true,
              value: selectedRadio == index,
              controlAffinity: ListTileControlAffinity.trailing,
              onChanged: (bool value) {
                setState(() {
                  if (selectedRadio != index) {
                    selectedRadio = index;
                  }

                  _canNext = true;
                });
              },
              activeColor: Colors.white,
              title: Text(surveyQuestion.questions.elementAt(index)),
            );
          } else {
            return Container(child: Text("Bu sorunun tipi algılanamadı!"));
          }
        },
        itemCount: widget.questionList.elementAt(questionNo).questions.length,
      );
    }
  }

  Map<int, dynamic> enteredInputs = Map<int, dynamic>();
  Set<int> filledTextFields = Set();
  List<TextEditingController> _editingControllers = List();

  Widget _buildInputTextTile(SurveyQuestion surveyQuestion, int index) {
    if (surveyQuestion.questions.length > _editingControllers.length)
      _editingControllers.add(TextEditingController());

    return ListTile(
      contentPadding: EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 8.0,
      ),
      leading: TextField(
        onChanged: (String value) {
          setState(() {
            if (value.isEmpty) {
              filledTextFields.remove(index);
            } else {
              if (!filledTextFields.contains(index)) {
                filledTextFields.add(index);
              }
            }

            if (filledTextFields.length < surveyQuestion.questions.length) {
              _canNext = false;
            } else {
              _canNext = true;
            }
          });
        },
        controller: _editingControllers[index],
        cursorColor: Colors.black,
        keyboardType: _keyboardType(surveyQuestion.inputType.elementAt(index)),
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black54),
            borderRadius: BorderRadius.all(
              Radius.circular(5.0),
            ),
          ),
          hintText: surveyQuestion.questions.elementAt(index),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _editingControllers.forEach((TextEditingController controller) {
      controller.dispose();
    });
  }

  String _comboBoxSelector(int index) {
    if (_isFirstComboSelected) {
      print("first selected");
      if (_selectedComboList.elementAt(index).isNotEmpty) {
        print("***** index = " + index.toString());
        print(_selectedComboList.elementAt(index));
        return _selectedComboList.elementAt(index);
      } else {
        return null;
      }
    } else {
      if (_selectedComboList.elementAt(0).isNotEmpty) {
        return _selectedComboList.elementAt(0);
      } else {
        return null;
      }
    }
  }
}
