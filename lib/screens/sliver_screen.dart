import 'package:flutter/material.dart';

class SliverScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SliverScreenState();
  }
}

class _SliverScreenState extends State<SliverScreen> {
  int _selectedTab = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      bottomNavigationBar: _buildBottomNavigationBar(),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            centerTitle: true,
            pinned: true,
            expandedHeight: 100.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text("Slivers"),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return Container(
                alignment: Alignment.center,
                color: Colors.teal[100 * (index % 9)],
                child: Text(
                  "Grid item $index",
                  style: TextStyle(fontSize: 12.0, color: Colors.black),
                ),
              );
            }, childCount: 20),
          ),
          SliverFixedExtentList(
            itemExtent: 50.0,
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return Container(
                alignment: Alignment.center,
                color: Colors.lightBlue[100 * (index % 9)],
                child: Text("list item $index"),
              );
            }),
          )
        ],
      ),
    );
  }

  Widget _buildBottomNavigationBar() {
    return Theme(
      isMaterialAppTheme: true,
      data: Theme.of(context).copyWith(
        canvasColor: Colors.black,
        textTheme: TextTheme(caption: TextStyle(color: Colors.blueGrey))
      ),
      child: BottomNavigationBar(
        currentIndex: _selectedTab,
        fixedColor: Colors.green,
        type: BottomNavigationBarType.fixed,
        items: [
          _buildNavigationBarItem(
              icon: Icon(Icons.home), title: "Home", index: 0),
          _buildNavigationBarItem(
              icon: Icon(Icons.search), title: "Search", index: 1),
          _buildNavigationBarItem(
              icon: Icon(Icons.map), title: "Map", index: 2),
          _buildNavigationBarItem(
              icon: Icon(Icons.person), title: "Profile", index: 3),
        ],
        onTap: _onSelectTab,
      ),
    );
  }

  _buildNavigationBarItem({Icon icon, String title, int index}) {
    return BottomNavigationBarItem(
        icon: icon,
        title: Text(
          title,
          style: TextStyle(color: _colorTabMatching(index)),
        ));
  }

  Color _colorTabMatching(int index) {
    return index == _selectedTab ? Colors.green : Colors.blueGrey;
  }

  void _onSelectTab(int value) {

    setState(() {
      _selectedTab = value;
    });

  _showOverlay(context);
  }

  void _showOverlay(BuildContext context) {
    Navigator.of(context).push(TutorialOverlay());
  }
}




class TutorialOverlay extends ModalRoute<void> {
  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'This is a nice overlay',
            style: TextStyle(color: Colors.white, fontSize: 30.0),
          ),
          RaisedButton(
            onPressed: () => Navigator.pop(context),
            child: Text('Dismiss'),
          )
        ],
      ),
    );
  }

  @override
  Widget buildTransitions(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }
}

