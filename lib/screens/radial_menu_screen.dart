import 'package:flutter/material.dart';
import '../custom_widgets/radial_menu.dart';

class RadialMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
        ),
        body: SizedBox.expand(child: RadialMenu()));
  }
}
