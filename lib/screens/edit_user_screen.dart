import 'package:flutter/material.dart';
import 'package:flutter_minimall_test/models/user.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';

class EditUserScreen extends StatefulWidget {
  final User user;

  EditUserScreen(this.user);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditUserScreenState();
  }
}

class _EditUserScreenState extends State<EditUserScreen> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _surnameController = TextEditingController();
  int _ageValue;
  File _image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController.text = widget.user.name;
    _surnameController.text = widget.user.surname;
    _ageValue = widget.user.age;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Kullanıcı Düzenleme"),
          bottom: TabBar(tabs: <Widget>[
            Tab(
              text: "First Tab",
              icon: Icon(Icons.looks_one),
            ),
            Tab(
              text: "Second Tab",
              icon: Icon(Icons.looks_two),
            ),
            Tab(
              text: "Third Tab",
              icon: Icon(Icons.looks_3),
            ),
          ]),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                //save
                Map<String, dynamic> user = Map();
                if (widget.user.name != _nameController.text) {
                  user.putIfAbsent("name", () => _nameController.text);
                }
                if (widget.user.surname != _nameController.text) {
                  user.putIfAbsent("surname", () => _surnameController.text);
                }
                if (widget.user.age != _ageValue) {
                  user.putIfAbsent("age", () => _ageValue);
                }

                if (user.isNotEmpty) {
                  DocumentReference ref = Firestore.instance
                      .collection("user")
                      .document(widget.user.id);
                  ref.updateData(user).timeout(Duration(seconds: 5)).then((_) {
                    print("Güncellendi");
                    Navigator.pop(context);
                  });
                } else {
                  print("Herhangi bir değişiklik yapılmadı");
                  Navigator.pop(context);
                }
              },
              icon: Icon(Icons.check),
            )
          ],
        ),
        body: TabBarView(children: [
          _buildEditUserTab(),
          _buildEditUserTab(),
          _buildEditUserTab(),
        ]),
      ),
    );
  }

  Widget _buildEditUserTab() {
    return ListView(
      padding: EdgeInsets.only(top: 16.0, bottom: 4.0),
      children: <Widget>[
        GestureDetector(
          onTap: () => _openImagePicker,
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 2.0, color: Colors.grey),
                ),
                child: CircleAvatar(
                    radius: 70.0,
                    backgroundImage: _image == null ? null : FileImage(_image),
                    backgroundColor: Colors.yellow[400],
                    child: _image == null
                        ? Text(
                        widget.user.name.toUpperCase().substring(0, 1) +
                            widget.user.surname
                                .toUpperCase()
                                .substring(0, 1),
                        style:
                        TextStyle(color: Colors.black, fontSize: 42.0))
                        : null),
              ),
            ],
          ),
        ),
        FlatButton(
            onPressed: () => _openImagePicker(context),
            child: Text(
              "Profil Resmini Değiştir",
              style: TextStyle(color: Colors.blue),
            )),
        _buildNameTextField(),
        Divider(
          height: 3.0,
          color: Colors.grey,
        ),
        _buildSurnameTextField(),
        Divider(
          height: 3.0,
          color: Colors.grey,
        ),
        SizedBox(height: 4.0),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Yaş",
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
        ),
        _buildAgePicker(),
        Divider(
          height: 3.0,
          color: Colors.grey,
        ),
      ],
    );
  }

  Widget _buildNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        maxLines: 1,
        controller: _nameController,
        decoration: InputDecoration(
          labelStyle: TextStyle(color: Colors.black),
          enabledBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          labelText: "Ad",
        ),
        validator: (String value) {},
        onSaved: (String value) {
          print("Ad kaydedildi: $value");
        },
      ),
    );
  }

  Widget _buildSurnameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        maxLines: 1,
        controller: _surnameController,
        decoration: InputDecoration(
          labelStyle: TextStyle(color: Colors.black),
          enabledBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          labelText: "Soyad",
        ),
        validator: (String value) {},
        onSaved: (String value) {
          print("Soyad kaydedildi: $value");
        },
      ),
    );
  }

  Widget _buildAgePicker() {
    return NumberPicker.integer(
        step: 1,
        infiniteLoop: true,
        initialValue: _ageValue,
        minValue: 0,
        maxValue: 120,
        onChanged: (value) {
          setState(() {
            _ageValue = value;
          });
        });
  }

  void _openImagePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: 160,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(
                  "Pick an Image",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  textColor: Theme
                      .of(context)
                      .primaryColor,
                  child: Text("Camera"),
                  onPressed: () {
                    _pickImage(context, ImageSource.camera);
                  },
                ),
                FlatButton(
                  textColor: Theme
                      .of(context)
                      .primaryColor,
                  child: Text("Gallery"),
                  onPressed: () {
                    _pickImage(context, ImageSource.gallery);
                  },
                )
              ],
            ),
          );
        });
  }

  Future _pickImage(BuildContext context, ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);

    setState(() {
      _image = image;
    });
    Navigator.pop(context);
  }
}
