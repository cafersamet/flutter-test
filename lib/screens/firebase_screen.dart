import 'package:flutter/material.dart';

//import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/user.dart';
import '../presentations/custom_icons_icons.dart';
import '../user_list_view.dart';

class FirebaseScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FirebaseScreenState();
  }
}

class _FirebaseScreenState extends State<FirebaseScreen> {
  TextEditingController _nameEditingController = TextEditingController();
  TextEditingController _surnameEditingController = TextEditingController();
  TextEditingController _ageEditingController = TextEditingController();

  TextEditingController _minAgeEditingController = TextEditingController();
  TextEditingController _maxAgeEditingController = TextEditingController();

//  DatabaseReference databaseReference = FirebaseDatabase.instance.reference();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int _selectedSortType = 0;
  bool _isDescendingOrder = false;
  bool _isSorted = false;
  SortType _sortType = SortType.NAME;
  FilterType _filterType = FilterType(maxAge: 130, minAge: 0);
  final Map<int, String> _sortingParameters = {
    0: "Sıralamayı seçiniz",
    1: "İsme göre A'dan Z'ye",
    2: "İsme göre Z'den A'ya",
    3: "Soyisme A'dan Z'ye",
    4: "Soyisme Z'den A'ya",
    5: "Yaşa göre artan",
    6: "Yaşa göre azalan",
  };

  bool _isFiltered = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("This is Firebase 🔥"),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(labelText: "Name"),
                    controller: _nameEditingController,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "Surname"),
                    controller: _surnameEditingController,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "Age"),
                    controller: _ageEditingController,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 8.0),
                  decoration: ShapeDecoration(
                      color: Colors.deepPurpleAccent, shape: CircleBorder()),
                  child: IconButton(
                    color: Colors.white,
                    iconSize: 30.0,
                    icon: Icon(CustomIcons.filter),
                    onPressed: () {
                      _showFilteringDialog();
                    },
                  ),
                ),
                Column(
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        _addUser();
                      },
                      child: Text("Add User"),
                    ),
//                    RaisedButton(
//                      onPressed: () {
////                  _retrieveUsers();
//
//                        Firestore.instance
//                            .collection("user")
//                            .where("name", isGreaterThan: "Cafer")
//                            .snapshots()
//                            .listen((data) => data.documents.forEach((doc) {
//                                  print(doc["name"]);
//                                  print(doc["age"]);
//                                  print(doc["surname"] + "\n");
//                                }));
//                      },
//                      child: Text("Retrieve User"),
//                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 8.0),
                  decoration: ShapeDecoration(
                      color: Colors.deepPurpleAccent, shape: CircleBorder()),
                  child: IconButton(
                    color: Colors.white,
                    iconSize: 30.0,
                    icon: Icon(Icons.sort_by_alpha),
                    onPressed: () {
                      _showSortingDialog();
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 16.0,
            ),
            Expanded(
                child: UserListView(
              isDescending: _isDescendingOrder,
              sortType: _sortType,
              isSorted: _isSorted,
              isFiltered: _isFiltered,
              filterType: _filterType,
            ))
          ],
        ),
      ),
    );
  }

  Future<void> _addUser() async {
//    String uuidKey = databaseReference.push().key;

    String name = _nameEditingController.text.trim();
    String surname = _surnameEditingController.text.trim();
    String ageText = _ageEditingController.text.trim();
    if (name.compareTo("") != 0 &&
        surname.compareTo("") != 0 &&
        ageText.compareTo("") != 0) {
      int age;
      try {
        age = int.parse(ageText);
        if (age < 0) {
          _showSnackBar(_scaffoldKey.currentState,
              "Yaş alanını uygun şekilde doldurunuz!");
          return;
        }
      } catch (e) {
        _showSnackBar(
            _scaffoldKey.currentState, "Yaş alanını uygun şekilde doldurunuz!");
        return;
      }

      Map<String, dynamic> user = Map();
      user.putIfAbsent("name", () => _nameEditingController.text);
      user.putIfAbsent("surname", () => _surnameEditingController.text);
      user.putIfAbsent("age", () => age);


      await Firestore.instance
          .collection("user")
          .document()
          .setData(user)
          .then((_) {
        _nameEditingController.text = "";
        _surnameEditingController.text = "";
        _ageEditingController.text = "";
        _showSnackBar(_scaffoldKey.currentState, "Kullanıcı eklendi!");
      });
    } else {
      _showSnackBar(_scaffoldKey.currentState, "Tüm alanları doldurunuz!");
    }
  }

  void _showSnackBar(ScaffoldState scaffoldState, String message) {
    final scaffold = scaffoldState;
    scaffold.showSnackBar(SnackBar(
      duration: Duration(seconds: 3),
      content: Text(message),
      action: SnackBarAction(
          label: "Tamam", onPressed: scaffold.hideCurrentSnackBar),
    ));
  }

  void _showSortingDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Sırala"),
            content: getAllSortListTile(),
            contentPadding: EdgeInsets.all(4.0),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Kapat"))
            ],
          );
        });
  }

  Widget getAllSortListTile() {
    return Container(
      width: double.maxFinite,
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return RadioListTile(
              title: Text(_sortingParameters.values.toList().elementAt(index)),
              value: _selectedSortType == index,
              groupValue: true,
              onChanged: (bool value) {
                print(index);
                setState(() {
                  if (_selectedSortType != index) _selectedSortType = index;
                  if (index == 0) {
                    _isSorted = false;
                  } else {
                    _isSorted = true;
                    if (index == 1) {
                      _isDescendingOrder = false;
                      _sortType = SortType.NAME;
                    } else if (index == 2) {
                      _isDescendingOrder = true;
                      _sortType = SortType.NAME;
                    } else if (index == 3) {
                      _isDescendingOrder = false;
                      _sortType = SortType.SURNAME;
                    } else if (index == 4) {
                      _isDescendingOrder = true;
                      _sortType = SortType.SURNAME;
                    } else if (index == 5) {
                      _isDescendingOrder = false;
                      _sortType = SortType.AGE;
                    } else {
                      _isDescendingOrder = true;
                      _sortType = SortType.AGE;
                    }
                  }
                });
                Navigator.pop(context);
              });
        },
        itemCount: _sortingParameters.length,
      ),
    );
  }

  void _showFilteringDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Filtrele"),
            content: getAllFilterList(),
            actions: <Widget>[
              FlatButton(
                child: Text("Sıfırla"),
                onPressed: () {
                  _minAgeEditingController.text = "";
                  _maxAgeEditingController.text = "";
                  setState(() {
                    _isFiltered = false;
                  });
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Uygula"),
                onPressed: () => _onApplyPressed(),
              ),
              FlatButton(
                child: Text("Kapat"),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          );
        });
  }

  Widget getAllFilterList() {
    return Container(
      width: double.maxFinite,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            "Yaş",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 4.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: TextField(
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: false, signed: false),
                  controller: _minAgeEditingController,
                  decoration: InputDecoration(
                      hintText: "Min Yaş",
                      border: OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 1.0, color: Colors.grey))),
                ),
              ),
              Icon(Icons.remove),
              Flexible(
                child: TextField(
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: false, signed: false),
                  controller: _maxAgeEditingController,
                  decoration: InputDecoration(
                    hintText: "Max Yaş",
                    border: OutlineInputBorder(
                        borderSide: BorderSide(width: 1.0, color: Colors.grey)),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  void _onApplyPressed() {
    String minAgeText = _minAgeEditingController.text.trim();
    String maxAgeText = _maxAgeEditingController.text.trim();

    if (maxAgeText.isNotEmpty && minAgeText.isNotEmpty) {
      int minAge, maxAge;

      try {
        minAge = int.parse(minAgeText);
        maxAge = int.parse(maxAgeText);
      } catch (e) {
        _showSnackBar(_scaffoldKey.currentState, "Lütfen sadece sayı giriniz!");
        return;
      }

      if (minAge < 0 || maxAge < 0) {
        _showSnackBar(_scaffoldKey.currentState, "Yaş bilgisi negatif olamaz!");
        return;
      } else if (minAge > maxAge) {
        _showSnackBar(_scaffoldKey.currentState,
            "Minimum yaş maksimum yaştan büyük olamaz");
        return;
      } else if (maxAge > 130) {
        _showSnackBar(_scaffoldKey.currentState,
            "Lütfen aralık olarak uygun bir yaş giriniz!");
        return;
      } else {
        setState(() {
          _isFiltered = true;
          _filterType = FilterType(minAge: minAge, maxAge: maxAge);
        });
        Navigator.pop(context);
      }
    } else {
      Navigator.pop(context);
    }
  }

//  void _retrieveUsers() async {
//    await databaseReference
//        .child("users")
//        .orderByChild("age")
//        .once()
//        .then((dataSnapshot) {
//      print(dataSnapshot.key);
//      print(dataSnapshot.value);
//      Map<dynamic, dynamic> users = dataSnapshot.value;
//      if (users.isNotEmpty) {
//        users.forEach((key, values) {
//          print(key);
//          print(values);
//          print("*******************");
//          User user = User();
//          user.name = values["name"];
//          user.surname = values["surname"];
//          user.age = values["age"];
//          setState(() {
//            userList.add(user);
//          });
//        });
//        userList.sort((user1, user2) => user1.name.compareTo(user2.name));
//      } else {
//        setState(() {
//          userList.clear();
//        });
//      }
//    });
//  }
}
