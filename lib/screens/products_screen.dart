import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../presentations/custom_icons_icons.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import '../sliding_route/slide_right_route.dart';
import 'package:flutter_minimall_test/screens/product_detail_screen.dart';
import 'dart:async';

class ProductsScreen extends StatelessWidget {
  final String image =
      "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-1_medium.jpg";
  final String logo =
      "https://upload.wikimedia.org/wikipedia/en/thumb/d/de/USPoloAssn_logo.jpg/220px-USPoloAssn_logo.jpg";
  final String title = "Regular Triko Kazak";
  final String price = "99,99";

  final List<String> imageList = [
    "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-1_medium.jpg",
    "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-2_medium.jpg",
    "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-3_medium.jpg",
    "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-5_medium.jpg",
    "https://img-uspoloassn.mncdn.com/UPLOAD/PRODUCT_NEW/2004x3006/08/1/0TK/thumb/50202072-KR0215-erkek-triko-kazak-6_medium.jpg"
  ];

  final List<String> colorList = ["Kırmızı", "Mavi", "Yeşil", "Sarı"];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text("Ürünler")),
        body: RefreshIndicator(
            backgroundColor: Colors.black,
            color: Colors.white,
            onRefresh: refreshList,
            child: ListView.builder(
                itemCount: 10,
                itemExtent: 160,
                padding: EdgeInsets.all(4.0),
                itemBuilder: (context, index) {
                  return GestureDetector(
                      onTap: () => Navigator.push(
                          context,
                          SlideRoute(
                              widget: ProductDetailScreen(
                            imageList: imageList,
                            colorList: colorList,
                          ))),
                      child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Card(
                              elevation: 5.0,
                              child: Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Row(children: <Widget>[
                                    //Expanded Image Card
                                    _buildImageCard(),
                                    _buildRightSide()
                                  ])))));
                })));
  }

  Widget _buildImageCard() {
    return Expanded(
        flex: 2,
        child: Card(
            elevation: 3.0,
            child: CachedNetworkImage(
                imageUrl: image,
                fit: BoxFit.fitWidth,
                placeholder: (context, url) =>
                    Center(child: CircularProgressIndicator()))));
  }

  Widget _buildRightSide() {
    return Expanded(
        flex: 5,
        child: Padding(
            padding: EdgeInsets.all(4.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildTitle(),
                  SizedBox(height: 8.0),
                  _buildBottomPart()
                ])));
  }

  Widget _buildTitle() {
    return Text(title,
        textAlign: TextAlign.left,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.black, fontSize: 21.0));
  }

  Widget _buildBottomPart() {
    return Expanded(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
          Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: 160.0,
                    child: SmoothStarRating(
                        allowHalfRating: true,
                        size: 32.0,
                        color: Colors.lightBlue,
                        borderColor: Colors.grey,
                        starCount: 5,
                        rating: 4.8)),
                SizedBox(height: 16.0),
                Row(children: <Widget>[
                  Icon(CustomIcons.lira, size: 21.0, color: Colors.green),
                  RichText(
                      maxLines: 1,
                      text: TextSpan(children: [
                        TextSpan(
                            text: '99,',
                            style: TextStyle(
                                color: Colors.green[700], fontSize: 21.0)),
                        TextSpan(
                            text: '99',
                            style: TextStyle(
                                color: Colors.green[700], fontSize: 16.0))
                      ]))
                ])
              ]),
          Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 3.0, color: Colors.grey)),
              child: CircleAvatar(
                  radius: 40.0,
                  backgroundImage: CachedNetworkImageProvider(logo)))
        ]));
  }

  Future<Null> refreshList() async {
    await Future.delayed(Duration(seconds: 1));
    return null;
  }
}
