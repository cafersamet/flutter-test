import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';
import '../custom_widgets/radial_menu.dart';

class FlareScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FlareScreenState();
  }
}

class _FlareScreenState extends State<FlareScreen> {
  String _animationStatus;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationStatus = "play";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Flare Screen"),
        backgroundColor: Colors.red[900],
      ),
      body: Container(
        color: Colors.orange[900],
        child: ListView(
          children: <Widget>[
            Container(
              height: 400,
              child: FlareActor(
                "assets/star_animation.flr",
                animation: _animationStatus,
                fit: BoxFit.fitHeight,
              ),
            ),
            RaisedButton(
              onPressed: () {
                String tempStatus =
                    _animationStatus == "play" ? "stop" : "play";
                setState(() {
                  _animationStatus = tempStatus;
                });
              },
              child: Text("Stop / Start"),
            ),
          ],
        ),
      ),
    );
  }
}
