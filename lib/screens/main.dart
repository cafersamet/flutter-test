import 'package:flutter/material.dart';
import 'package:flutter_minimall_test/sliding_route/slide_right_route.dart';
import 'package:flutter_minimall_test/screens/web_view_screen.dart';
import 'package:flutter_minimall_test/screens/survey_screen.dart';
import 'package:flutter_minimall_test/screens/sliver_screen.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_minimall_test/screens/firebase_screen.dart';
import '../expandable_list.dart';
import 'package:flutter_minimall_test/screens/home_screen.dart';
import 'package:flutter_minimall_test/screens/flare_screen.dart';
import 'package:flutter_minimall_test/screens/radial_menu_screen.dart';
import 'edit_user_screen.dart';
import 'local_auth_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(
        seconds: 5,
        gradientBackground: LinearGradient(
            colors: [Colors.cyan, Colors.blue],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight),
        navigateAfterSeconds: HomeScreen(),
        title: Text(
          'Welcome In SplashScreen',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        image: Image.network(
            'https://flutter.io/images/catalog-widget-placeholder.png'),
//        backgroundGradient: new LinearGradient(
//            colors: [Colors.cyan, Colors.blue],
//            begin: Alignment.topLeft,
//            end: Alignment.bottomRight),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Flutter Egypt"),
        loaderColor: Colors.red,
      ),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/home':
            return SlideRoute(widget: SurveyScreen(0));
            break;
          case '/myhome':
            return SlideRoute(widget: HomeScreen(), isRight: false);
            break;
          case '/webView':
            return ScaleRoute(widget: WebViewScreen());
            break;
          case '/sliver':
            return ScaleRoute(widget: SliverScreen());
            break;
          case '/firebase':
            return SlideRoute(widget: FirebaseScreen());
            break;
          case '/expandable':
            return SlideRoute(widget: ExpandableList());
            break;
          case '/flare':
            return SlideRoute(widget: FlareScreen());
            break;
          case '/radial':
            return SlideRoute(widget: RadialMenuScreen());
            break;
          case '/editUser':
            return SlideRoute(widget: EditUserScreen(settings.arguments));
            break;
          case '/localAuth':
            return SlideRoute(widget: LocalAuthScreen());
            break;
        }
      },
    );
  }
}
