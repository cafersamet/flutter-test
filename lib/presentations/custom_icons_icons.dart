/// Flutter icons MyFlutterApp
/// Copyright (C) 2019 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  MyFlutterApp
///      fonts:
///       - asset: fonts/MyFlutterApp.ttf
///
///
/// * Elusive, Copyright (C) 2013 by Aristeides Stathopoulos
///         Author:    Aristeides Stathopoulos
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://aristeides.com/
///
import 'package:flutter/widgets.dart';

class CustomIcons {
  CustomIcons._();

  static const _kFontFam = 'CustomIcons';

  static const IconData female = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData male = const IconData(0xe801, fontFamily: _kFontFam);
  static const IconData filter = const IconData(0xf0b0, fontFamily: _kFontFam);
  static const IconData gplus = const IconData(0xf0d5, fontFamily: _kFontFam);
  static const IconData lira = const IconData(0xf195, fontFamily: _kFontFam);
  static const IconData product_hunt =
      const IconData(0xf288, fontFamily: _kFontFam);
}
