import 'package:flutter/material.dart';
import 'package:flutter_minimall_test/survey_question.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SurveyImage extends StatefulWidget {
  final int questionNo;
  final SurveyQuestion surveyQuestion;

  SurveyImage(this.questionNo, this.surveyQuestion);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
}

class _SurveyImageState extends State<SurveyImage> {
  Set<int> _selectedImages;
  bool _canNext;
  final BorderSide _tappedBorder =
      BorderSide(color: Colors.blue[900], width: 5.0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedImages = Set();
    _canNext = false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    //isImage = true;
    return GridView.builder(
        itemCount: widget.surveyQuestion.questions.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 4.0,
            childAspectRatio: 1.0,
            crossAxisSpacing: 4.0),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                if (_selectedImages.contains(index)) {
                  _selectedImages.remove(index);
                } else {
                  _selectedImages.add(index);
                }

                _selectedImages.length > 0 ? _canNext = true : _canNext = false;
              });
            },
            child: Card(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  side: _selectedImages.contains(index)
                      ? _tappedBorder
                      : BorderSide(width: 0.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                  Center(
                    child: CachedNetworkImage(
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      imageUrl:
                          widget.surveyQuestion.questions.elementAt(index),
                    ),
                  )
                ],
              ),
            ),
          );
        });
    ;
  }
}
