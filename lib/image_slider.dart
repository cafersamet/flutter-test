import 'package:flutter/material.dart';
import 'page_slider/slider.dart';
import 'package:swipedetector/swipedetector.dart';

class ImageSliderScreen extends StatelessWidget {
  final List<String> imageList;
  final int index;

  ImageSliderScreen(this.imageList, this.index);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
          child: SwipeDetector(
            onSwipeDown: () {
              Navigator.of(context).pop();
            },
            child: Stack(children: <Widget>[
              Hero(tag: index,
                child: CustomImageSlider(imageList: imageList,heroIndex: index,),
              ),
              Container(
                  height: 48,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Color(0x66000000),
                            Color(0x33000000),
                            Color(0x00000000)
                          ])),
                  child: AppBar(
                    backgroundColor: Color(0x00121212), leading: CloseButton(),))
            ]),
          ),
        ));
  }
}
