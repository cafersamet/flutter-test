import 'package:flutter/material.dart';

class ProgressDialog extends StatefulWidget {
  final bool loading;
  final String imagePath;
  _ProgressDialogState state;

  // key eklenecek

  ProgressDialog(
      {this.imagePath = "assets/minimall.png", this.loading = false});

  @override
  _ProgressDialogState createState() {
    // TODO: implement createState
    state = _ProgressDialogState();
    return state;
  }
}

class _ProgressDialogState extends State<ProgressDialog>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  bool _visible = false;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController);
    _visible = widget.loading;
  }

  void dismiss() {
    setState(() {
      this._visible = false;
    });
  }

  void show() {
    setState(() {
      this._visible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_visible) {
      _animationController.forward();
      _animationController.repeat();
      return Scaffold(
        backgroundColor: Colors.black26,
        body: Center(
          child: Container(
            height: MediaQuery.of(context).size.width * 0.25,
            width: MediaQuery.of(context).size.width * 0.25,
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            child: FadeTransition(
              opacity: _animation,
              child: Image.asset(
                "assets/minimall.png",
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }
}
