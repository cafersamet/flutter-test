class SurveyQuestion {
  String type;
  List<String> questions;
  String text;
  List<String> inputType;
  List<Map<String, List<String>>> comboValues;

  SurveyQuestion(this.type, this.questions, this.text,
      {this.inputType, this.comboValues});

  static List<SurveyQuestion> questionList = [
    SurveyQuestion(
        "image",
        [
          "http://www.smile.com.tr/baskili-desenli-spor-erkek-gomlek-2-2636-44-K.jpg",
          "https://cds.sorsbt.com/kip/ContentImages/Product/2017-2/10088262/uzun-kollu-gomlek_beyaz-beyaz_1_buyuk.JPG",
          "http://www.pembe.org/wp-content/uploads/2017/01/Bordo-kareli-abbate-gomlek-modelleri.jpg",
          "http://www.smile.com.tr/baskili-desenli-spor-erkek-gomlek-2-2636-44-K.jpg",
          "https://cds.sorsbt.com/kip/ContentImages/Product/2017-2/10088262/uzun-kollu-gomlek_beyaz-beyaz_1_buyuk.JPG",
          "http://www.pembe.org/wp-content/uploads/2017/01/Bordo-kareli-abbate-gomlek-modelleri.jpg",
        ],
        "Size uygun olan gömlekleri seçiniz."),
    SurveyQuestion(
        "radio",
        [
          "5-15",
          "15-30",
          "30'dan fazla",
        ],
        "Bir mağazada ortalama kaç dakika vakit harcıyorsunuz?"),
    SurveyQuestion(
        "check",
        [
          "Mavi",
          "Kırmızı",
          "Yeşil",
          "Siyah",
          "Beyaz",
          "Sarı",
          "Turuncu",
        ],
        "Sevdiğiniz renkleri seçiniz."),
    SurveyQuestion(
        "input",
        [
          "Boy(cm)",
          "Kilo(kg)",
          "Tuttuğunuz Takım",
        ],
        "Aşağıdaki bilgileri uygun şekilde doldurunuz.",
        inputType: [
          "number",
          "number",
          "text",
        ]),
    SurveyQuestion(
        "combo",
        [
          "İl",
          "İlçe",
        ],
        "Nerede yaşıyorsunuz?",
        comboValues: [
          {
            "İstanbul": [
              "Üsküdar",
              "Ümraniye",
              "Ataşehir",
              "Kadıköy",
              "Şile",
            ]
          },
          {
            "Ankara": [
              "Çankaya",
              "Keçiören",
              "Sincan",
              "Yenimahalle",
            ]
          },
          {
            "Bursa": [
              "İnegöl",
              "Osmangazi",
              "Gemlik",
            ]
          },
          {
            "İzmir": [
              "Çeşme",
              "Buca",
              "Bornova",
              "Karşıyaka",
              "Konak",
            ]
          },
          {
            "Erzurum": [
              "Yakutiye",
              "Aziziye",
              "Tortum",
              "Aşkale",
            ]
          },
          {
            "Konya": [
              "Çumra",
              "Selçuklu",
              "Ereğli",
              "Beyşehir",
            ]
          },
        ]),
  ];
}
